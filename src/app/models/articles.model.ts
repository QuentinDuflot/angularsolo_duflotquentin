export interface Article {
    id: number;
    titre: string;
    description: string;
    detail: string;
    pathImage: string;
}
