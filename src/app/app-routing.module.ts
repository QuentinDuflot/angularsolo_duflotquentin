import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from '../app/home-page/home-page.component';
import { ListeCartesComponent } from '../app/liste-cartes/liste-cartes.component';
import { ContactComponent } from '../app/contact/contact.component';
import { DetailArticleComponent } from './detail-article/detail-article.component';
import { CartesByRaceComponent } from './cartes-by-race/cartes-by-race.component';
import { CartesByClassComponent } from './cartes-by-class/cartes-by-class.component';
import { CartesBySearchComponent } from './cartes-by-search/cartes-by-search.component';


const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'home-page', component: HomePageComponent },
  { path: 'liste-cartes', component: ListeCartesComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'detail-article/:id', component: DetailArticleComponent },
  { path: 'cartes-by-race/:id', component: CartesByRaceComponent },
  { path: 'cartes-by-class/:id', component: CartesByClassComponent },
  { path: 'cartes-by-search/:id', component: CartesBySearchComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
