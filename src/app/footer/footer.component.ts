import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  twitter: string;
  facebook: string;
  mail: string;
  constructor() {
    this.twitter = 'https://twitter.com/hearthstone_fr?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor';
    this.facebook = 'https://www.facebook.com/Hearthstone.fr/';
    this.mail = 'mailto:duflotquentin@hotmail.fr';
   }

  ngOnInit() {
  }

}
