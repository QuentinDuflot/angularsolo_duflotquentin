import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ListeCartesComponent } from './liste-cartes/liste-cartes.component';
import { ContactComponent } from './contact/contact.component';
import { DetailArticleComponent } from './detail-article/detail-article.component';
import { CartesByRaceComponent } from './cartes-by-race/cartes-by-race.component';
import { CartesByClassComponent } from './cartes-by-class/cartes-by-class.component';
import { CartesBySearchComponent } from './cartes-by-search/cartes-by-search.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavbarComponent,
    FooterComponent,
    HomePageComponent,
    ListeCartesComponent,
    ContactComponent,
    DetailArticleComponent,
    CartesByRaceComponent,
    CartesByClassComponent,
    CartesBySearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
