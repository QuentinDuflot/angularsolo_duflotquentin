import { Injectable } from '@angular/core';
import { Article } from '../models/articles.model';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  private students: Article[] = [
    {
      id: 0,
      titre: 'Un nerf à venir pour le Chaman ?',
      description: 'Trouvez ici toutes les possibilités pour le futur nerf du chaman',
      // tslint:disable-next-line: max-line-length
      detail: 'L\'extension L\'Envol des Dragons est à peine sortie depuis deux jours complets que l\'équipe de développement Hearthstone s\'est vue "contrainte" d\'annoncer des changements très rapides sur certaines cartes. Si tous les détails ne sont pas encore connus, il y aura forcément "quelques changements destinés au Chaman" comme l\'annonce le tweet ci-dessous. En effet, depuis ce 10 décembre, ce deck martyrise la plupart des joueurs en ladder, avec une decklist déjà très optimisée et extrêmement polyvalente et puissante, le Chaman Galakrond. Il est assez rare pour un deck d\'avoir aussi peu de failles, et le winrate est même monté à plus de 67 % après quelques dizaines d\'heures. Plusieurs pistes sont envisageables au niveau des modifications, comme le pouvoir héroïque de Galakrond, la Tempête, ou encore le sort Meute des dragons par exemple. On pourrait aussi penser à l\'Elémentaliste corrompue ou aux 8/8 qui sont invoquées par Galakrond lors de son arrivée. Il y a tellement d\'axes de nerfs possibles que la team 5 trouvera de quoi faire ! Dernière chose : ce patch arrive dans la semaine prochaine, donc ne vous impatientez pas trop !',
      pathImage: '../../assets/Images/nerf.jpg'
    },
    {
      id: 1,
      titre: 'Un nouveau héro alternatif pour le prêtre',
      description: 'Trouvez ici toutes les infos sur ce nouveau héro !',
      // tslint:disable-next-line: max-line-length
      detail: 'Si vous n\'aviez pas eu l\'occasion à l\'époque, vous pourrez bientôt invoquer la puissance d\'Elune avec Tyrande Murmevent dans Hearthstone ! L\'héroïne alternative Prêtre, ajoutée en jeu en 2016, représente la plus grande forme d\'autorité chez les Elfes de la Nuit. La prêtresse est aussi la bien-aimée de Malfurion Hurlorage, le héros Druide.',
      pathImage: '../../assets/Images/tyrande.png'
    },
    {
      id: 2,
      titre: 'Bientôt une nouvelle aventure solo !',
      description: 'La prochaine aventure arrivera un janvier !',
      // tslint:disable-next-line: max-line-length
      detail: 'Les deux modes solo de cette Année du Dragon ont été parmi les plus aboutis et plus plaisants à jouer depuis le début des aventures solo Hearthstone. Le Casse du Siècle avait notamment une durée de vie et une rejouabilité incroyable, tandis que les Tombes de la Terreur avaient amené de très belles nouveautés vraiment intéressantes. L\'Envol des Dragons aura aussi son mode solo qui sortira au courant du mois de janvier !',
      pathImage: '../../assets/Images/solo.jpg'
    }
  ];

  constructor() {
  }

  getAllArticles(): Article[] {
    return this.students;
  }

  getOneArticle(id: number) {
    return this.students.find(student => student.id === id);
  }
}
