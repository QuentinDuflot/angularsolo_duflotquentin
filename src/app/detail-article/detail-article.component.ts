import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../services/article.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-article',
  templateUrl: './detail-article.component.html',
  styleUrls: ['./detail-article.component.scss']
})
export class DetailArticleComponent implements OnInit {
  titre: string;
  description: string;
  detail: string;
  pathImage: string;

  constructor(private articleService: ArticleService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.titre = this.articleService.getOneArticle(+id).titre;
    this.detail = this.articleService.getOneArticle(+id).detail;
    this.description = this.articleService.getOneArticle(+id).description;
    this.pathImage = this.articleService.getOneArticle(+id).pathImage;
  }

}
