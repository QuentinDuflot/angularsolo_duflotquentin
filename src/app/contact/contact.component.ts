import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  name: string;
  email: string;
  message: string;
  constructor() { }

  ngOnInit() {
  }
  processForm() {
    // tslint:disable-next-line: max-line-length
    const allInfo = 'Je m\'appelle: ' + this.name + '. Ceci est mon adresse mail: ' + this.email + '. Voici mon message: ' + this.message;
    alert(allInfo);
  }

}
