import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartesByRaceComponent } from './cartes-by-race.component';

describe('CartesByRaceComponent', () => {
  let component: CartesByRaceComponent;
  let fixture: ComponentFixture<CartesByRaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartesByRaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartesByRaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
