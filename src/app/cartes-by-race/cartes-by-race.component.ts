import { Component, OnInit } from '@angular/core';
import { Race } from '../models/race.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cartes-by-race',
  templateUrl: './cartes-by-race.component.html',
  styleUrls: ['./cartes-by-race.component.scss']
})
export class CartesByRaceComponent implements OnInit {
  race: Race[] = [];
  id = this.route.snapshot.params['id'];
  settings = {
    // tslint:disable: object-literal-key-quotes
    'async': true,
    'crossDomain': true,
    'url': 'https://omgvamp-hearthstone-v1.p.rapidapi.com/cards/races/' + this.id,
    'method': 'GET',
    'headers': {
      'x-rapidapi-host': 'omgvamp-hearthstone-v1.p.rapidapi.com',
      'x-rapidapi-key': 'a54f1fade3mshcbb45d28ddfbdf6p18e15ajsnf99799d6a85a'
    }
  };
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    // tslint:disable-next-line: only-arrow-functions
    $.ajax(this.settings).done((response) => {
      response.forEach(element => {
        this.race.push({name: element.name, img: element.img});
      });
      console.log(this.race);
    });
  }

}


