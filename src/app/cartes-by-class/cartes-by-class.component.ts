import { Component, OnInit } from '@angular/core';
import { Class } from '../models/class.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cartes-by-class',
  templateUrl: './cartes-by-class.component.html',
  styleUrls: ['./cartes-by-class.component.scss']
})

export class CartesByClassComponent implements OnInit {
  class: Class[] = [];
  id = this.route.snapshot.params['id'];
  settings = {
    // tslint:disable: object-literal-key-quotes
    'async': true,
    'crossDomain': true,
    'url': 'https://omgvamp-hearthstone-v1.p.rapidapi.com/cards/classes/' + this.id,
    'method': 'GET',
    'headers': {
      'x-rapidapi-host': 'omgvamp-hearthstone-v1.p.rapidapi.com',
      'x-rapidapi-key': 'a54f1fade3mshcbb45d28ddfbdf6p18e15ajsnf99799d6a85a'
    }
  };
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    // tslint:disable-next-line: only-arrow-functions
    $.ajax(this.settings).done((response) => {
      response.forEach(element => {
        this.class.push({name: element.name, img: element.img});
      });
      console.log(this.class);
    });
  }

}



