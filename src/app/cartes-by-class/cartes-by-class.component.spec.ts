import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartesByClassComponent } from './cartes-by-class.component';

describe('CartesByClassComponent', () => {
  let component: CartesByClassComponent;
  let fixture: ComponentFixture<CartesByClassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartesByClassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartesByClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
