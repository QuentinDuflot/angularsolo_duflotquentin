import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  pathImage: string;
  link1: string;
  link2: string;
  link3: string;
  cardSearch: string;


  constructor() {
    this.link1 = 'Page d\'accueil';
    this.link2 = 'Liste des cartes';
    this.link3 = 'Nous contacter';
    this.pathImage = '../../assets/Images/logo.png';
  }

  onSubmit() {
    window.location.assign('/cartes-by-search/' + this.cardSearch);

  }

  ngOnInit() {
  }


}
