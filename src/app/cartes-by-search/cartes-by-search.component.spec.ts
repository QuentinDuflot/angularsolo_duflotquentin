import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartesBySearchComponent } from './cartes-by-search.component';

describe('CartesBySearchComponent', () => {
  let component: CartesBySearchComponent;
  let fixture: ComponentFixture<CartesBySearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartesBySearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartesBySearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
