import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Search } from '../models/search.model';

@Component({
  selector: 'app-cartes-by-search',
  templateUrl: './cartes-by-search.component.html',
  styleUrls: ['./cartes-by-search.component.scss']
})

export class CartesBySearchComponent implements OnInit {
  search: Search[] = [];
  id = this.route.snapshot.params['id'];
  settings = {
    // tslint:disable: object-literal-key-quotes
    'async': true,
    'crossDomain': true,
    'url': 'https://omgvamp-hearthstone-v1.p.rapidapi.com/cards/search/' + this.id,
    'method': 'GET',
    'headers': {
      'x-rapidapi-host': 'omgvamp-hearthstone-v1.p.rapidapi.com',
      'x-rapidapi-key': 'a54f1fade3mshcbb45d28ddfbdf6p18e15ajsnf99799d6a85a'
    }
  };
  constructor(private route: ActivatedRoute) { }
  ngOnInit() {
    // tslint:disable-next-line: only-arrow-functions
    $.ajax(this.settings).done((response) => {
      response.forEach(element => {
        this.search.push({name: element.name, img: element.img});
      });
      console.log(this.search);
    });
  }

}



