import { Component, OnInit } from '@angular/core';
import { Info } from '../models/info.model';

@Component({
  selector: 'app-liste-cartes',
  templateUrl: './liste-cartes.component.html',
  styleUrls: ['./liste-cartes.component.scss']
})
export class ListeCartesComponent implements OnInit {
  info: Info[] = [];
  settings = {
    // tslint:disable: object-literal-key-quotes
    'async': true,
    'crossDomain': true,
    'url': 'https://omgvamp-hearthstone-v1.p.rapidapi.com/info',
    'method': 'GET',
    'headers': {
      'x-rapidapi-host': 'omgvamp-hearthstone-v1.p.rapidapi.com',
      'x-rapidapi-key': 'a54f1fade3mshcbb45d28ddfbdf6p18e15ajsnf99799d6a85a'
    }
  };
  constructor() { }

  ngOnInit() {
    // tslint:disable-next-line: only-arrow-functions
    $.ajax(this.settings).done((response) => {
      this.info['classes'] = response.classes;
      this.info['race'] = response.races;
    });
  }

}
